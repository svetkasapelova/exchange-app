
# Crypto exchange

App that shows stats for currency exchange rates for Bitcoin, Etherium, LiteCoin and Ripple

Statisticts periods are: last 1hour, last 24h, last day, last 7days, last year

## To get the frontend running locally

- install node.js
- install npm

- Clone this repo
- ``` npm install ``` or ``` yarn install ``` to install dependencies
- ``` npm start ```  or ``` yarn start ``` to start the local server 

## Description

When the app is first loaded it 4 parrallel calls to the API https://cex.dev.sowalabs.com/api/history/cache/${currency}?periods=${period}` where currency is 4 currency listed above, and periods param is one of the following [
    1min, 5min, 30min, 30min, 2hrs, 1d
]

And shows 4 graph data for each currency.
Each 3 minutes the API is called again and the graph are updated in case of any change in the response data

Codebase: React + Redux.

### State management

We use Redux for the state managent, and Redux-saga middleware for async calls and other side effects.

https://github.com/redux-saga/redux-saga/tree/master/docs/api

Redux saga is built on javascript generator functions. The main benefits of redux saga: it allows to write async code in a syncronized way, what makes it look simpler and it's easy to manage concurrent and periodical async actions, and async racing calls.

In our case we call saga that starts calling API periodically. When we need to change the the periods query for the API call, we cancel the previous saga and start all over again

Example:

When the action is dispathced, then call api and set a timer to pint it automatically every 3 minutes

``` 
 yield takeEvery(FETCH_EXCHANGE_PRICE, startFetchingPrice)
``` 
takeEvery will listen for each FETCH_EXCHANGE_PRICE action and call startFetchingPrice generator

``` 
function* startFetchingPrice({currency, period}) {
    if (startNewFetchTask) {
        yield cancel(startNewFetchTask)
    }
    startNewFetchTask =  yield fork(fetchExchangePrice, {currency, period})
}
``` 
startFetchingPrice first checks if the timeout function is running, cancel it, and calls fetchExchangePrice
generator

``` 
function* fetchExchangePrice({currency, period}) {
    while(true) {
        yield call(fetchPriceData, {currency, period})
        yield call(delay, 180000)
    }
}
``` 
fetchExchangePrice calls generator that calls API with 3 minutes interval of 3 minutes

### Css

We use sass modules to style components. Each component folder has a styles.scss file with the styles.

We use react-flexbox-grid  package that provides a set of React components implementing flexboxgrid.
It is easy to use and possible to customize with components or css modules.
https://github.com/roylee0704/react-flexbox-grid

Example:

``` 
 <Grid fluid>
    <Row>
        <Col xs={6} md={3}>
        Hello, world!
        </Col>
    </Row>
</Grid>
``` 

### Selectors

We use memoized selectors from reselect package (https://github.com/reduxjs/reselect) to pass part of the store to the components and calculate derived data. Memoized selectors help to avoid unnecessary rerender of the components on every store change. Also we isolate all the logic needed to calculated derived data to the selectors, so that components are kept as dumb as possible 

#### Derivation data from API

App receives data in a format:

```
  [  
   {  
      "updateTime":"2018-06-01T09:32:18.4699206Z",
      "masterCloseTime":"0001-01-01T00:00:00",
      "period":"02:00:00",
      "timeSpan":"30.00:00:00",
      "items":[  
         {  
            "closeTime":"2018-05-02T10:00:00Z",
            "open":7597.3,
            "high":7616.8,
            "low":7584.0,
            "close":7591.8,
            "volume":232.73036
         },
         {  
            "closeTime":"2018-05-02T12:00:00Z",
            "open":7598.8,
            "high":7655.5,
            "low":7591.7,
            "close":7635.1,
            "volume":544.23444
         },
         ......

   }
]  
```
The response is stored in the Redux state in the format for each currency pair:

```
    {
        abbr: {                                 # one of the following ['btceur', 'ltceur', 'etheur', 'xrpeur']
            status: '',                         # possible values:  'LOADED' || 'LOADING' || 'FAILED'
            items:  [],                         # row server data
            updateTime: '',                     # row server data
            period: '',                         # row server data
            timeSpan: '',                       # row server data
            masterCloseTime: ''                 # row server data
        }
    }
```

In selectors we process each item in items array to derive the data to be used by the the stock chart, and passed this data to the components are props.

The data we derive are:

-  Change in close price within the given period

```
   diffEur = items[items.length - 1].close - items[0].close
```

- Percentage change in the close price

```
   diffPercent = (diffEur / items[0].close) * 100
```

- Human readabale dates for each item to be shown on the chart and tooltip

```
    {
        ...
        closeTime: "2018-05-02T10:00:00Z",
        closeTimeFormat: "2 May",
        closeTimeFullDate: "2 May 10:00:00", 
    } 

```

- interval parameter for the chart axis (we want up to 6 ticks)

```
    interval = Math.round(length / 6),
```


Example of the derived data for 'btceur' pair:

```
    {
        btceur: {                               
            status: 'LOADED',              
            items: [
                ...
                {   
                    close: 565.3,
                    closeTime: "2018-05-02T10:00:00Z",     
                    closeTimeFormat: "2 May",                 # human readable data for the X axis on the graph
                    closeTimeFullDate: "2 May 10:00:00",      # human readable data for the tooltip
                    high: 568,
                    low: 567,
                    close: 568,
                    volume: 2526,

                }
                ...
            ]
            updateTime: '',                                        
            period: '',                                             
            timeSpan: '',                                           
            masterCloseTime: ''                                     
            title:"2 May 2018 12:00 UTC - 1 Jun 2018 12:00 UTC"     # human readable title for the graph
            diffEur: -64.43                                         # change of the close price
            diffPercent: -11.4                                      # percentage change of the close price
            infoPriceEnd: 500.87                                    # last close price
            interval: 50                                            # interval for the Xaxis ticks
        }
    }
```


### Project structure


```
.app
  ├──api                            # calls to API
  ├──components                     # folder with presentational and container components
        ├──CoinGraph                # component to display graph data
                ├──container.js     # component that connect graph component to the store and actions
                ├──index.js         # graph component to show graph data
                ├──styles.scss      # styles for graph 
          ├──Dashboard              # component to display 4 graphs
                ├──container.js     # component that connect Dashboard component to the store and actions
                ├──index.js         # Dashboard presentational component 
                ├──styles.scss      # styles for Dashboard
          ├──Loader                 # Simple css powered loader to show when data are not available
                ├──index.js         # Loader  component 
                ├──styles.scss      # styles for Loader 
          ├──Header         
                ├──container.js     # component that connect Header
                ├──index.js         # Header presentational component 
                ├──styles.scss      # styles for Header

  ├──helpers                        # helper functions 
  ├──config                         # config for different environents
  ├──store
        ├──actions                  # redux actions
        ├──reduces                # redux reducers
        ├──sagas                  # redux-saga generators
        ├──selectors              # memoized selectors to compute derived data from the state
  ├── styles                      # global styles used by all components
  ├── public                      # public files
  ├── scripts                     # scripts to run tests, dev server and build the production bundle
  └── README.md
```
