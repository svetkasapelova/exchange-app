/**
 * All api calls to the server
 */
import axios from 'axios'
export const fetch = (url, method = 'GET') =>  () => {
    return new Promise(((resolve, reject) => 
        axios.request(url, method)
        .then(result => resolve(...result.data))
        .catch(error => reject(error))))
    }