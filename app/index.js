import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import DashboardContainer from './components/Dashboard/container'
import HeaderContainer from './components/Header/container'
import { Provider } from 'react-redux'
import store from './store'


ReactDOM.render(
     <Provider store={store}>
        <React.Fragment>
            <HeaderContainer />
            <DashboardContainer />
        </React.Fragment>
    </Provider>, document.getElementById('root'));

