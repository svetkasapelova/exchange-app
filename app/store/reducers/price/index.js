import update from 'immutability-helper';
import { 
    FETCH_EXCHANGE_PRICE_REQUEST,
    FETCH_EXCHANGE_PRICE_FAILURE,
    FETCH_EXCHANGE_PRICE_SUCCESS,
    CHANGE_EXCHANGE_PRICE_PERIOD } from '../../actions/price'
import { supportedCoins, exchangePeriod } from '../../../helpers'

// load mock data 
const initialState = {
    activePeriod: 3,  //active period index
    periods: exchangePeriod,
    currencies: supportedCoins
}
// initialise data obj for all supported coins
// we avoid extra rendering because of this
supportedCoins.forEach(coin => initialState[coin] = {abbr: coin})

export default function(state = initialState, action) {
    switch(action.type) {       
        case FETCH_EXCHANGE_PRICE_REQUEST:
            return update(state, {[action.currency]: {status: {$set: 'LOADING'}}})
        case FETCH_EXCHANGE_PRICE_FAILURE:
            return update(state, {[action.currency]: {status: {$set: 'FAILED'}}})
        case FETCH_EXCHANGE_PRICE_SUCCESS:
            return update(state, {[action.currency]: {$set: {status: 'LOADED', ...action.data, abbr: action.currency}}})
        case CHANGE_EXCHANGE_PRICE_PERIOD:
            return update(state, {$set: {...initialState, activePeriod: action.period }})
        default:
            return state
    }
}