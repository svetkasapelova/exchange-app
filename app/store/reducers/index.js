import { combineReducers } from 'redux'
import price from './price'

/**
 * We don't need to use combine reducers in this case, 
 * but I will leave it for the sake of the real world example
 * where we will have multiple reducers
 */

export default combineReducers({
  price,
})