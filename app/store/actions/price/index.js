 /**
  * List of action names for price data
  * */ 
 const NAMESPACE =  'price/'
 export const CHANGE_EXCHANGE_PRICE_PERIOD = `${NAMESPACE}CHANGE_EXCHANGE_PRICE_PERIOD`
 export const FETCH_EXCHANGE_PRICE = `${NAMESPACE}FETCH_EXCHANGE_PRICE`
 export const FETCH_EXCHANGE_PRICE_REQUEST = `${NAMESPACE}FETCH_EXCHANGE_PRICE_REQUEST`
 export const FETCH_EXCHANGE_PRICE_SUCCESS = `${NAMESPACE}FETCH_EXCHANGE_PRICE_SUCCESS`
 export const FETCH_EXCHANGE_PRICE_FAILURE = `${NAMESPACE}FETCH_EXCHANGE_PRICE_FAILURE`

 /**
  * Actions
  */

/**
 * Fetch price data from the api
 */
export const fecthExchangePrice = ({currency, periods}) => ({
    type: FETCH_EXCHANGE_PRICE,
    currency,
    periods,
})

/**
 * Change the time periods for the api calls
 */
export const changePricePeriod = ({period}) => ({
    type: CHANGE_EXCHANGE_PRICE_PERIOD,
    period,
})
