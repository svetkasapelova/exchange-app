
import moment from 'moment'

import { createSelector } from 'reselect'
import { getCurrencyName, round } from '../../../helpers'
import update from 'immutability-helper';

const getCurrencyData = (state, props) => state.price[props.currency]

export const allCurrPrice = state => state.price

//we need to create instance of selector for each graph componet 
//so we return a function instead of a selector
export const makeGetCurrencyPrice = () => {
    return createSelector(getCurrencyData, (currency) =>  {
        if (!currency.items) {
            return currency
        }
        
        const length = currency.items.length
        const endCloseTime = moment(currency.items[length - 1].closeTime)
        const startCloseTime = moment(currency.items[0].closeTime)
        const dateDiff = moment(endCloseTime).diff(startCloseTime)
        const duration = moment.duration(dateDiff).days()
    
        //calculate currency close diff within the given period
        const priceStart = currency.items[0].close
        const priceEnd = currency.items[length - 1].close
        const diffEur = priceEnd - priceStart
        const diffPercent = (diffEur / priceStart) * 100
        const updateItems = currency.items.map(item => {
            const utcCloseTime = moment.utc(item.closeTime)
             return update(item, {$set: 
                {...item, 
                closeTimeFormat: duration > 1 && utcCloseTime.format('D MMM') || utcCloseTime.format('HH:ss:mm'),
                closeTimeFullDate: utcCloseTime.format('D MMM HH:ss:mm')}
            })
        })
        
        return update(currency, {$set: 
            {
                ...currency, 
                diffPercent: round(diffPercent),
                diffEur: round(diffEur),
                interval: Math.round(length / 6),
                name: getCurrencyName(currency.abbr),
                infoPriceEnd: round(priceEnd),
                title: `${startCloseTime.format('D MMM Y HH:SS')} UTC - ${endCloseTime.format('D MMM Y HH:SS')} UTC`,
                items: updateItems,
            }})
    })
}
