/* main app saga */
import { fork, all } from 'redux-saga/effects' 

import priceSaga from './price'

export default function* root() { 
    yield all([ 
            fork(priceSaga) 
        ])
}