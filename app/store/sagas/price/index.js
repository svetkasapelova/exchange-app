/**
 * Reducers to pull price data from API
 */

import { delay } from 'redux-saga'
import { call, put, takeEvery, fork, cancel } from 'redux-saga/effects'
import {
        FETCH_EXCHANGE_PRICE,
        FETCH_EXCHANGE_PRICE_REQUEST, 
        FETCH_EXCHANGE_PRICE_FAILURE, 
        FETCH_EXCHANGE_PRICE_SUCCESS } from '../../actions/price'

import { fetch } from '../../../api'
/**
 * Saga for fetching price.
 * Make a call to API and emit Success/Failure actions
 */
function* fetchPriceData({currency, periods}) {
    try { 
       yield put({type: FETCH_EXCHANGE_PRICE_REQUEST, currency})
       const data = yield call(fetch(`https://cex.dev.sowalabs.com/api/history/cache/${currency}?periods=${periods}`))
       yield put({type: FETCH_EXCHANGE_PRICE_SUCCESS, data, currency})
    } catch (e) {
       yield put({type: FETCH_EXCHANGE_PRICE_FAILURE, message: e.message})
    }
 }

 /**
  * Saga that pings api periodically. Once per 2 mins
  *
  */
function* fetchExchangePrice({currency, periods}) {
    while(true) {
        yield call(fetchPriceData, {currency, periods})
        yield call(delay, 180000)
    }
}

/**
 * StartFetchingPrice starts a new task that pings API every 3 mins 
 * per currenct per periods, and storea the ref to the task task in startNewFetchTask array.
 * When a new FETCH_EXCHANGE_PRICE action for the same currency is dispatched
 * We need to cancel previeous task for this currency and start new task.
 */
let startNewFetchTask = []

function* startFetchingPrice({currency, periods}) {
    if (startNewFetchTask[currency]) {
        yield cancel(startNewFetchTask[currency])
    }
    startNewFetchTask[currency] =  yield fork(fetchExchangePrice, {currency, periods})
}

// cancel preview calls to api and start new calls
export default function* priceSaga() {
    yield takeEvery(FETCH_EXCHANGE_PRICE, startFetchingPrice)
  }



