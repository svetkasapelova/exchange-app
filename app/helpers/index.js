// list of supported coins and exchange period time. We need it for initializing the state
export const supportedCoins = ['btceur', 'ltceur', 'etheur', 'xrpeur']

export const exchangePeriod = [
    {
        duration: '1H',
        periods: '1m'
    },
    {
        duration: '24H',
        periods: '5m'
    },
    {
        duration: '7D',
        periods: '30m'
    },
    {
        duration: '30D',
        periods: '2h'
    },
    {
        duration: '12M',
        periods: '1d'
    },
]

// hardcode the list of currency names, since we dont receive them from API
export const getCurrencyName = (abbr) => {
    return abbr === 'btceur' && 'Bitcoin (BTC)' 
        || abbr === 'ltceur' && 'Litecoin (LTC)' 
        || abbr === 'etheur' && 'Ether (ETH)' 
        || abbr === 'xrpeur' && 'Ripple (XPR)'
}
/**
 * Round number till 2 decimals or till the first not zero decimal
 */
export const round = (number, index = 100) => {
    if (number === 0) {
        return number
    }
    let roundedNumber = Math.round(number * index) / index
    while(roundedNumber === 0 && index < 10000000000) {
       roundedNumber = Math.round(number * index) / index
       index *= 10
    }
    return roundedNumber
}
