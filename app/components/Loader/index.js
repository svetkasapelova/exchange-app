import React, { Component } from 'react'
import styles from './styles.scss'

export const Loader = () => <div className={styles.loader}></div>
