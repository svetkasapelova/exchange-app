import React, { Component } from 'react'
import CoinGraphContainer from '../CoinGraph/container'
import styles from './styles.scss'

import { Row, Col } from 'react-flexbox-grid'

const Dashboard = (props) => {
      const { price } = props
      return (
          <Row center="xs" className={styles.container} >
              {price.currencies.map(cur => 
                <Col className={styles.graph} md={12} lg={5} key={`${cur}_${price.activePeriod}`} >
                  <CoinGraphContainer currency={cur} periods={price.periods[price.activePeriod].periods} />
                </Col>
              )}
          </Row>
      )
    }
  
  export default Dashboard