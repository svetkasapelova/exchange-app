/**
 * Container for the CoinGraph component
 * It connects the component to the store, and maps actions to props
 */

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { allCurrPrice } from '../../store/selectors/price'
import { changePricePeriod } from '../../store/actions/price'
import Dashboard from '../Dashboard/'

const mapStateToProps = (state, props) => {
    return {
      price: allCurrPrice(state)
    }
  }

export default connect(
    mapStateToProps, 
)(Dashboard)