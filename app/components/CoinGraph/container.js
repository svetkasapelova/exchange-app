/**
 * Container for the CoinGraph component
 * It connects the component to the store, and maps actions to props
 */

import { connect } from 'react-redux'
import { Grid,Row, Col } from 'react-flexbox-grid'

import { makeGetCurrencyPrice } from '../../store/selectors/price'
import { fecthExchangePrice } from '../../store/actions/price'
import CoinGraph from '../CoinGraph/'

const makeMapStateToProps = (state, props) => {
    const getCurrencyPrice = makeGetCurrencyPrice()

    //return a function in order to create memoized selector 
    //for each component instance to avoid rerendering on every state change
    const mapStateToProps = (state, props) => {
      return {
        data: getCurrencyPrice(state, {currency: props.currency})
      }
    }
    return mapStateToProps
  }

const mapDispatchToProps = (dispatch, props) => {
    return {
      init: () => {
        dispatch(fecthExchangePrice({currency: props.currency, periods: props.periods}))
      }
    }
}

export default connect(
  makeMapStateToProps,
  mapDispatchToProps   
)(CoinGraph)