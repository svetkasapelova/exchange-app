/**
 * Display single graph for exchane price data
 */
import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { AreaChart, 
        XAxis, 
        Tooltip, 
        Area, 
        CartesianGrid, 
        YAxis, 
        ResponsiveContainer 
    } from 'recharts'

import { Loader } from '../Loader'
import { Row, Col } from 'react-flexbox-grid'

import styles from './styles.scss'


const toolTipContent = (item, value, props) => {
   const {payload} = props
    return  <React.Fragment>
                <Col xs={12} className={styles.row}>{payload.closeTimeFullDate} &#8364;{payload.close}</Col>
                <Col xs={12} className={styles.row}>low: &#8364;{payload.low} </Col>
                <Col xs={12} className={styles.row}>high: &#8364;{payload.high} </Col>
            </React.Fragment>

}


export default class CoinGraph extends Component {
    static propTypes = {
        data: PropTypes.shape({
            items: PropTypes.array,
            status: PropTypes.string
          })
    }
    componentDidMount(){
        this.props.init()
    }


    render() {
        const { data } = this.props

        if (!data.items){
            return  <Col xs={12}><Loader /></Col>
        }
        const classPriceDiff = data.diffEur > 0 && 'pos' || 'neg'
        const priceEnd = `\u20AC ${data.infoPriceEnd}`
        const percentDiff= `${data.diffPercent}%`
        const euroDiff = `\u20AC ${data.diffEur}`

        return (
            <React.Fragment>
                <Col xs={12} className={styles.row}>{data.title}</Col>
                <Col xs={12} className={styles.row}>{data.name} {priceEnd}</Col>
                <Col xs={12} className={`${styles.row} ${styles[classPriceDiff]}`}>{percentDiff} ({euroDiff})</Col>
                <ResponsiveContainer height="60%">
                    <AreaChart
                        data={data.items}
                        margin={{ top: 5, right: 20, left: 10, bottom: 5 }}
                    >
                    <defs>
                        <linearGradient id="colorGrad" x1="0" y1="0" x2="0" y2="1">
                            <stop offset="5%" stopColor="rgb(17, 68, 103)" stopOpacity={0.8}/>
                            <stop offset="95%" stopColor="rgb(17, 68, 103)" stopOpacity={0}/>
                        </linearGradient>
                    </defs>
                        <XAxis dataKey="closeTimeFormat" interval={data.interval} tick={{fontSize: 12}}/>
                        <YAxis dataKey="close" tick={{fontSize: 12}}/>
                        <Tooltip formatter={toolTipContent}/>
                        <CartesianGrid stroke="#f5f5f5" />
                        <Area type="monotone" dataKey="close" stroke="rgb(17, 68, 103)" fillOpacity={1} fill="url(#colorGrad)" />
                    </AreaChart>
                </ResponsiveContainer>
            </React.Fragment>    
        )
    }
}