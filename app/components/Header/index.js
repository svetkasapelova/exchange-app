import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styles from './styles.scss'

export default class Header extends PureComponent {
    static propTypes = {
        price: PropTypes.shape({
            activePeriod: PropTypes.number,
            periods: PropTypes.array
        })  
    }
    render() {
        const { price } = this.props
        return (
            <div className={styles.header}>
                <div className={styles.nav}>
                {price.periods.map((p, index) => {
                    const active = index === price.activePeriod
                    return  <button disabled={active} className={`${styles.navbutton} ${active&& styles.active}`} key={p.duration}  
                    onClick={() => this.props.changePricePeriod({period: index})}>{p.duration}</button>
                    }
                )}  
                </div>
            </div>
        )
    }
}