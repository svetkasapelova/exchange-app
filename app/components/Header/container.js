/**
 * Container for the Header component
 * It connects the component to the store, and maps actions to props
 */

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { allCurrPrice } from '../../store/selectors/price'
import { changePricePeriod } from '../../store/actions/price'

import Header from '../Header/'

const mapStateToProps = (state, props) => {
    return {
      price: allCurrPrice(state)
    }
  }

const mapDispatchToProps = (dispatch) => 
    bindActionCreators(
        {
            changePricePeriod
        },
        dispatch  
)

export default connect(
    mapStateToProps,
    mapDispatchToProps   
)(Header)